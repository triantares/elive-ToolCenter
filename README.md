# Elive Tool Center

A paned verion of the tabbed tools center to try out visual design options.

<img src="https://i.imgur.com/LyVUv38.png" style="zoom:20%;" />

On a 13" 2560x1440 screen

<img src="https://i.imgur.com/OMXd1I8.png" style="zoom:15%;" />

On a 22" 3840x2160 screen

<img src="https://i.imgur.com/QfTte3R.png" style="zoom:50%;" />

On a  10" 1024x760 screen.


**Points to note:**

- tab2 (the one on the bottom) uses the available space in the window.....so put images and such where size matters there.
- The fonts on the field-buttons (FBTN) can be changed in a limited way with 'markup/HTML' tags. Manipulating (icon)size and background is hard but probably can be done using 'css' (which would get in the way of adhering to set DE themes.)

- ~~Maybe a probe into the available 'screensize' (xrandr?) and setting (calculating) size and postion (of the main window)from there?~~ Done!

  **To test:**

- check if the top pane can use notebook style (tabs) inside the paned window.

- See if image in top or bottom tab can be changed without reloading whole window.

- ~~See what 'nice' options are available using tooltips (--notification) i.e 'on hover' options.~~ Done!





After adding checks for screen resolution and sizing the window and internals (i.e icons) in relation to the screen, it looks like this on 1024x576:















